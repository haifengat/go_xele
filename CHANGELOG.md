# 更新记录

## v0.1.0

- 新增: ReqOrderInsertFAK2 用合约指针委托(避免 instrument map 错误)
- 修复: InstrumentPoint 应转为 uint64
- 修复: XFT_D_Sell String 返回值
- 新增: TradePro Release 函数
- 修复: 多次 NewTrade 时不释放 t 导致的错误
- 新增: OnExchange, OnEvent, OnError 响应提示
- 新增: Instruments 保存合约的指针
- 新增: OnLoadFinished 登录成功后调用一次 OnAccount
- 新增: localID -> LocalID
- 更新: reqOrder 即使错误也返回 localID
- 新增: getErrorMessage 读取错误码的中文说明
- 新增: updateBook 更新合约的行情(接口没有行情推送,需通过此函数触发 onUpdateBook)
- 新增: subscribe 行情订阅
- 新增: ReqOrderInsert 返回 localID
- 新增: getVersion 取 api 版本号
- 新增: TradePro 进一步封装连接,登录,委托,撤单
- 新增: cancelOrder 使用 locadID 撤单
- 新增: insertOrder 增加 instrument 指针参数,以避免重复查询的性能问题
- 修复: OnOrder 响应需在 loadFinished 响应后才生效
- 新增: insertOrder 发单正常,但无响应(待修复)
- 修复: float64 和 uint32 在 cgo 转换后读取字节数不对(float64:10),采用自定义类型解决
- 新增: dataType 增加 String 函数, 以方便 print
- 新增: getInstrumentByID, 发单前查询对应的合约
- 基础函数与响应 golang 封装
