package main

import (
	"os"
	"testing"
)

func TestMain(t *testing.T) {
	// 资金账号
	os.Setenv("ACCOUNT_ID", "41015541")
	// 账号密码
	os.Setenv("ACCOUNT_PWD", "111111")
	// 看穿式监管需要的APP_ID
	os.Setenv("APP_ID", "client_TraderAPI_4.1")
	// 看穿式监管需要的授权码
	os.Setenv("AUTH_CODE", "f0b3c35302f5828f")
	// 查询使用的地址和端口（TCP）
	os.Setenv("QUERY_SERVER_IP", "218.94.105.246")
	os.Setenv("QUERY_SERVER_PORT", "53333")
	// 交易使用的地址和端口（UDP）
	os.Setenv("TRADE_SERVER_IP", "218.94.105.246")
	os.Setenv("TRADE_SERVER_PORT", "62000")
	main()
}
