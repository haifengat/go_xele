# 艾科朗克

## 介绍

艾科朗克接口

## 安装教程

1.  复制 lib 目录到程序目录下

## 使用说明

```go
package goxele

import (
	"fmt"
	"testing"
	"time"
)

func TestPro(t *testing.T) {
	fmt.Println("starting ...")
	p := NewTradePro()
	p.OnOrder = func(errorCode int, order *XTFOrder) {
		if !order.IsHistory {
			fmt.Printf("%+v\n", order)
		}
	}
	p.OnTrade = func(trade *XTFTrade) {
		if !trade.IsHistory {
			fmt.Printf("%+v\n", trade)
		}
	}
	p.OnCancelOrder = func(errorCode int, cancelOrder *XTFOrder) {
		if !cancelOrder.IsHistory {
			fmt.Printf("%+v\n", cancelOrder)
		}
	}
	p.OnBookUpdate = func(book *XTFMarketData) {
		fmt.Printf("%+v\n", book)
	}

	err := p.ReqConnect("218.94.105.246", 62000, 53333)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("连接成功")
	err = p.ReqLogin("41015541", "111111", "client_TraderAPI_4.1", "f0b3c35302f5828f")
	if err != nil {
		fmt.Println(err)
		return
	}

	res := p.Subscribe("rb230")
	if res != 0 {
		fmt.Println(res, p.GetErrorMessage(res))
		return
	}
	time.Sleep(3 * time.Second)
	p.UpdateBook("rb2305", 1, 2, 3, 4, 5)
	// id, err := p.ReqOrderInsert("rb2305", XFT_D_Sell, XTF_OF_Open, 11680, 5)
	// if err != nil {
	// 	fmt.Println(err)
	// 	return
	// }
	// fmt.Println("委托成功, localID: ", id)
	// time.Sleep(1 * time.Second)
	// fmt.Println("撤单")
	// p.ReqCancelOrder()
	select {}
}
```
